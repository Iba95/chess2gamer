# README #

Ein WPF-Schachspiel mit 2 Spielern die auf einem lokalen PC spielen.
Abwechselnd weißer und schwarzer Spieler. Wenn auf eine Figur geklickt wird, leuchten die Felder auf, auf die er wechseln kann. 
Keine Einzelspielerfunktion. Es wird einen (Button) geben mit dem Content "Neustart" der das Spiel neu startet.
Geschlagene Schachfiguren werden grafisch neben dem Schachbrett dargestellt (links/rechts je nachdem). Timer der anzeigt wie lange ein
Spieler schon spielt oder auch wie lange er noch Zeit zum spielen hat.